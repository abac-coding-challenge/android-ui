package com.example.client.validator;


import java.util.regex.Pattern;

public class UserValidator {
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9_*+.-]{3,20}@[a-zA-z]{3,20}\\.[a-z]{2,3}$";
    private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-_#$^+=!*@%&]).{8,20}$";

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);

        return email != null && !email.equals("") && pattern.matcher(email).matches();
    }

    public static boolean validatePassword(String password) {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

        return password != null && !password.equals("") &&
                pattern.matcher(password).matches();
    }

    public static boolean validatePasswordConfirm(String password, String passwordConfirm) {
        return password != null && !password.equals("") &&
                passwordConfirm != null && !passwordConfirm.equals("") &&
                passwordConfirm.equals(password);
    }
}
