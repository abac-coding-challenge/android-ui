package com.example.client.util;

public class Constants {
        public static final String BASE_URL = "http://10.0.2.2:8080";

    public static final String HEADER_CONTENT_TYPE_KEY = "Content-Type";
    public static final String HEADER_CONTENT_TYPE_VALUE = "application/json";
    public static final String HEADER_AUTHORIZATION_KEY = "Authorization";
    public static final String HEADER_AUTHORIZATION_VALUE = "Bearer ";

    public static final String SHARED_PREFERENCE_FILE = "XPAND";
    public static final String SHARED_PREFERENCE_KEY = "AUTHORIZATION";
}
