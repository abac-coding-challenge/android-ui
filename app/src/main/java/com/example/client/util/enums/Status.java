package com.example.client.util.enums;

public enum Status {
    OK("OK"),
    NOT_OK("!OK"),
    TODO("TODO"),
    EN_ROUTE("En route");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    public static Status getEnumByValue(String value) {
        Status[] statuses = Status.values();

        for (Status status : statuses) {
            if (status.getValue().equals(value)) {
                return status;
            }
        }

        return null;
    }

    public String getValue() {
        return value;
    }
}
