package com.example.client.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.client.model.AuthorizationToken;
import com.google.gson.Gson;

import static com.example.client.util.Constants.SHARED_PREFERENCE_FILE;
import static com.example.client.util.Constants.SHARED_PREFERENCE_KEY;

public class SharedPreferencesUtil {
    public static void setAuthTokenValue(Context context, AuthorizationToken token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(token);

        editor.putString(SHARED_PREFERENCE_KEY, json);
        editor.apply();
    }

    public static AuthorizationToken getAuthTokenValue(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_FILE, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = sharedPreferences.getString(SHARED_PREFERENCE_KEY, "");

        return gson.fromJson(json, AuthorizationToken.class);
    }

    public static void removeSharedPref(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
