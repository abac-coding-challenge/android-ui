package com.example.client.util;

import android.content.Context;

import com.example.client.R;
import com.example.client.model.ErrorResponse;
import com.example.client.network.ServerAccessManager;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class ExceptionConverterUtil {
    public static String convertExceptionToMessage(ResponseBody response) {
        ErrorResponse message = null;

        Converter<ResponseBody, ErrorResponse> errorConverter =
                ServerAccessManager.getServerAccess().responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        try {
            message = errorConverter.convert(response);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (message != null) {
            return message.getError();
        }

        return "";
    }

    public static String getErrorMessage(Context context, String exceptionCode) {
        ExceptionCodes code;

        try {
            code = ExceptionCodes.valueOf(exceptionCode);
        } catch (Exception e) {
            return "Internal Server Error";
        }

        switch (code) {
            case ACCESS_DENIED:
                return context.getString(R.string.error_access_denied);
            case ALREADY_EXISTING_EMAIL:
                return context.getString(R.string.error_email_already_exits);
            case ALREADY_EXISTING_PLANET_NAME:
                return context.getString(R.string.error_planet_name_already_exits);
            case BAD_CREDENTIALS:
                return context.getString(R.string.error_bad_credentials);
            case EMAIL_OF_USER_DOES_NOT_MATCH_REQUIREMENTS:
            case INCORRECT_REFRESH_TOKEN:
            case INCORRECT_TOKEN_TYPE:
            case PASSWORD_OF_USER_DOES_NOT_MATCH_REQUIREMENTS:
            case PLANET_STATUS_IS_NULL_OR_IT_IS_MISTYPED:
            case ROLE_OF_USER_IS_INVALID:
                return context.getString(R.string.error_invalid_data);
            case EMAIL_OF_USER_IS_NULL:
            case PASSWORD_OF_USER_IS_NULL:
            case ROLE_OF_USER_IS_NULL_OR_IT_IS_MISTYPED:
            case USER_DTO_IS_NULL:
            case USER_IS_NULL:
                return context.getString(R.string.error_data_null);
            case CREW_NOT_FOUND_BY_ID:
                return context.getString(R.string.error_crew_not_found);
            case ROBOT_NOT_FOUND_BY_ID:
                return context.getString(R.string.error_robot_not_found);
            case PLANET_NOT_FOUND_BY_ID:
                return context.getString(R.string.error_planet_not_found);
            case USER_NOT_FOUND_BY_EMAIL:
            case USER_NOT_FOUND_BY_ID:
                return context.getString(R.string.error_user_not_found);
            case UNAUTHORIZED:
                return context.getString(R.string.error_unauthorized);
            default:
                return "Internal Server Error";
        }
    }

    public enum ExceptionCodes {
        ACCESS_DENIED("ACCESS_DENIED"),
        ALREADY_EXISTING_EMAIL("ALREADY_EXISTING_EMAIL"),
        ALREADY_EXISTING_PLANET_NAME("ALREADY_EXISTING_PLANET_NAME"),
        BAD_CREDENTIALS("BAD_CREDENTIALS"),
        CREW_NOT_FOUND_BY_ID("CREW_NOT_FOUND_BY_ID"),
        EMAIL_OF_USER_DOES_NOT_MATCH_REQUIREMENTS("EMAIL_OF_USER_DOES_NOT_MATCH_REQUIREMENTS"),
        EMAIL_OF_USER_IS_NULL("EMAIL_OF_USER_IS_NULL"),
        INCORRECT_REFRESH_TOKEN("INCORRECT_REFRESH_TOKEN"),
        INCORRECT_TOKEN_TYPE("INCORRECT_TOKEN_TYPE"),
        PASSWORD_OF_USER_DOES_NOT_MATCH_REQUIREMENTS("PASSWORD_OF_USER_DOES_NOT_MATCH_REQUIREMENTS"),
        PASSWORD_OF_USER_IS_NULL("PASSWORD_OF_USER_IS_NULL"),
        PLANET_NOT_FOUND_BY_ID("PLANET_NOT_FOUND_BY_ID"),
        PLANET_STATUS_IS_NULL_OR_IT_IS_MISTYPED("PLANET_STATUS_IS_NULL_OR_IT_IS_MISTYPED"),
        ROBOT_NOT_FOUND_BY_ID("ROBOT_NOT_FOUND_BY_ID"),
        ROLE_OF_USER_IS_INVALID("ROLE_OF_USER_IS_INVALID"),
        ROLE_OF_USER_IS_NULL_OR_IT_IS_MISTYPED("ROLE_OF_USER_IS_NULL_OR_IT_IS_MISTYPED"),
        UNAUTHORIZED("UNAUTHORIZED"),
        USER_DTO_IS_NULL("USER_DTO_IS_NULL"),
        USER_IS_NULL("USER_IS_NULL"),
        USER_NOT_FOUND_BY_EMAIL("USER_NOT_FOUND_BY_EMAIL"),
        USER_NOT_FOUND_BY_ID("USER_NOT_FOUND_BY_ID");

        private final String value;

        ExceptionCodes(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
