package com.example.client.util;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.example.client.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

public class ImageUtil {
    private static final int CAMERA_REQUEST_CODE = 1000;
    private static final int STORAGE_REQUEST_CODE = 1001;
    private static final int CAMERA_PERMISSION_CODE = 1888;
    private static final int STORAGE_PERMISSION_CODE = 1889;
    private static final String[] cameraPermissions =
            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final String[] storagePermissions =
            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final String FILE_PROVIDER = "com.example.client.fileprovider";
    private static String imagePath;

    public static boolean hasCameraPermission(Fragment fragment) {
        boolean permissionCamera = ContextCompat.checkSelfPermission(fragment.requireActivity(),
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

        // for high quality image have to save image to external storage first, then inserting to imageView
        boolean permissionStorage = ContextCompat.checkSelfPermission(fragment.requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

        return permissionCamera && permissionStorage;
    }

    public static void requestCameraPermission(Fragment fragment) {
        fragment.requestPermissions(cameraPermissions, CAMERA_PERMISSION_CODE);
    }

    public static void openCamera(Fragment fragment) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(fragment.requireActivity().getPackageManager()) != null) {
            File photoFile = null;

            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast.makeText(fragment.requireActivity(), R.string.error_camera_image, Toast.LENGTH_SHORT).show();
            }

            if (photoFile != null) {
                Uri imageUri = FileProvider.getUriForFile(
                        fragment.requireContext(),
                        FILE_PROVIDER,
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                fragment.startActivityForResult(intent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public static File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        ImageUtil.imagePath = image.getAbsolutePath();

        return image;
    }

    public static String getImagePathFromFile() {
        return ImageUtil.imagePath;
    }

    public static boolean hasStoragePermission(Fragment fragment) {
        return ContextCompat.checkSelfPermission(fragment.requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestStoragePermission(Fragment fragment) {
        fragment.requestPermissions(storagePermissions, STORAGE_PERMISSION_CODE);
    }

    public static void openStorage(Fragment fragment) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setDataAndType(EXTERNAL_CONTENT_URI, "image/*");
        fragment.startActivityForResult(intent, STORAGE_REQUEST_CODE);
    }

    public static String getImagePathFromUri(Fragment fragment, Uri imageUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String path = null;
        int column_index;

        Cursor cursor = fragment.requireActivity().getContentResolver()
                .query(imageUri, proj, null, null, null);

        if (cursor != null) {
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            path = cursor.getString(column_index);
            cursor.close();
        }

        return path;
    }

    public static Bitmap resizeImageBitmap(String imagePath) {
        final int newWidth = 300;
        final int newHeight = 300;

        File imgFile = new File(imagePath);
        Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - (float) bitmap.getWidth() / 2,
                middleY - (float) bitmap.getHeight() / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

    public static String encodeImageToString(String imagePath) {
        Bitmap bitmap = resizeImageBitmap(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();

        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public static Bitmap decodeImageFromString(String imageString) {
        byte[] imageBytes = Base64.decode(imageString, Base64.DEFAULT);

        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }
}
