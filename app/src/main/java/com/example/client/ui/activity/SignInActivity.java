package com.example.client.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.User;
import com.example.client.network.AuthorizationTokenApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromActivity;

public class SignInActivity extends AppCompatActivity {
    private AuthorizationTokenApi authorizationTokenApi;
    private TextInputLayout mEmailLayout, mPasswordLayout;
    private TextInputEditText mEmail, mPassword;
    private View mProgressDialog;
    private View mSignInForm;
    private ScrollView mScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        hideKeyboardFromActivity(this);

        this.authorizationTokenApi = ServerAccessManager.getServerAccess().create(AuthorizationTokenApi.class);
        this.mProgressDialog = findViewById(R.id.sign_in_progress);
        this.mSignInForm = findViewById(R.id.sign_in_form);
        this.mScroll = findViewById(R.id.sign_in_scroll);

        setUpInputFields();
        setUpButtons();
    }

    private void setUpInputFields() {
        this.mEmailLayout = findViewById(R.id.sign_in_email_layout);
        this.mEmail = findViewById(R.id.sign_in_email);
        this.mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mEmailLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.mPasswordLayout = findViewById(R.id.sign_in_password_layout);
        this.mPassword = findViewById(R.id.sign_in_password);
        this.mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPasswordLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setUpButtons() {
        Button btn_signIn = findViewById(R.id.btn_sign_in);
        btn_signIn.setOnClickListener(v -> {
            boolean validInputs = true;

            if (mPassword.getText() == null || mPassword.getText().toString().equals("")) {
                validInputs = false;
                mPasswordLayout.setError(getText(R.string.error_empty_passwords));
                mPassword.requestFocus();
            }

            if (mEmail.getText() == null || mEmail.getText().toString().equals("")) {
                validInputs = false;
                mEmailLayout.setError(getText(R.string.error_empty_email));
                mEmail.requestFocus();
            }

            if (validInputs) {
                signInCall(mEmail.getText().toString(), mPassword.getText().toString());
            }
        });

        Button btn_signUp = findViewById(R.id.btn_sign_up);
        btn_signUp.setOnClickListener(v -> {
            mScroll.pageScroll(View.FOCUS_UP);

            if (mEmail != null && mEmail.getText() != null) {
                mEmail.getText().clear();
                mEmail.clearFocus();
            }

            if (mPassword != null && mPassword.getText() != null) {
                mPassword.getText().clear();
                mPassword.clearFocus();
            }

            startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
        });
    }

    private void signInCall(final String email, final String password) {
        showProgressBar(true);
        User user = new User(email, password);
        Call<AuthorizationToken> call = authorizationTokenApi.create(user);
        call.enqueue(new Callback<AuthorizationToken>() {
            @Override
            public void onResponse(@NonNull Call<AuthorizationToken> call, @NonNull Response<AuthorizationToken> response) {
                showProgressBar(false);

                if (response.isSuccessful()) {
                    AuthorizationToken token = response.body();
                    if (token != null) {
                        SharedPreferencesUtil.setAuthTokenValue(SignInActivity.this, token);
                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                        finish();
                    }
                } else {
                    if (response.errorBody() != null) {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(SignInActivity.this, exceptionMessage);
                        Toast.makeText(SignInActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AuthorizationToken> call, @NonNull Throwable t) {
                showProgressBar(false);
                Toast.makeText(SignInActivity.this, R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressBar(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        mSignInForm.setVisibility(show ? View.GONE : View.VISIBLE);
        mSignInForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSignInForm.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

        mProgressDialog.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressDialog.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressDialog.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
    }
}
