package com.example.client.ui.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.Crew;
import com.example.client.model.Planet;
import com.example.client.network.AuthorizationTokenApi;
import com.example.client.network.CrewApi;
import com.example.client.network.PlanetApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.activity.SignInActivity;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.example.client.util.Constants.HEADER_AUTHORIZATION_VALUE;
import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromFragment;
import static com.example.client.util.ImageUtil.decodeImageFromString;
import static com.example.client.util.ImageUtil.encodeImageToString;
import static com.example.client.util.ImageUtil.getImagePathFromUri;
import static com.example.client.util.ImageUtil.hasCameraPermission;
import static com.example.client.util.ImageUtil.hasStoragePermission;
import static com.example.client.util.ImageUtil.openCamera;
import static com.example.client.util.ImageUtil.openStorage;
import static com.example.client.util.ImageUtil.requestCameraPermission;
import static com.example.client.util.ImageUtil.requestStoragePermission;
import static com.example.client.util.ImageUtil.resizeImageBitmap;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class DetailPlanetFragment extends Fragment {
    private static final String PLANET = "PLANET";
    private static final int CAMERA_PERMISSION_CODE = 1888;
    private static final int STORAGE_PERMISSION_CODE = 1889;
    private static final int CAMERA_REQUEST_CODE = 1000;
    private static final int STORAGE_REQUEST_CODE = 1001;

    private PlanetApi planetApi;
    private AuthorizationTokenApi authorizationTokenApi;
    private CrewApi crewApi;

    private Planet planet;
    private ScrollView mScroll;
    private ImageView mImageView;
    private TextInputLayout mNameLayout, mDescriptionLayout;
    private TextInputEditText mName, mDescription;
    private String mSelectedStatus, mSelectedCrew;
    private Spinner mCrew;
    private List<Crew> crews;
    private List<String> captains;

    private String mImagePath;

    public DetailPlanetFragment() {
    }

    public static DetailPlanetFragment newInstance(Planet planet) {
        DetailPlanetFragment fragment = new DetailPlanetFragment();
        Bundle args = new Bundle();
        args.putSerializable(PLANET, planet);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.planet = (Planet) getArguments().getSerializable(PLANET);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_planet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.planetApi = ServerAccessManager.getServerAccess().create(PlanetApi.class);
        this.authorizationTokenApi = ServerAccessManager.getServerAccess().create(AuthorizationTokenApi.class);
        this.crewApi = ServerAccessManager.getServerAccess().create(CrewApi.class);
        this.crews = new ArrayList<>();
        this.captains = new ArrayList<>();
        this.captains.add(getText(R.string.select_crew).toString());

        this.mScroll = view.findViewById(R.id.detail_planet_scroll);
        this.mImageView = view.findViewById(R.id.detail_planet_image_view);

        getAllCrewCall();
        setUpInputFields(view);
        setUpButtons(view);
        hideKeyboardFromFragment(requireContext(), view);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted && writeStorageAccepted) {
                        openCamera(DetailPlanetFragment.this);
                    } else {
                        Toast.makeText(requireActivity(), R.string.error_permission_denied, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case STORAGE_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (writeStorageAccepted) {
                        openStorage(DetailPlanetFragment.this);
                    } else {
                        Toast.makeText(requireActivity(), R.string.error_permission_denied, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        Uri mImageUri;
        Bitmap imageBitmap;

        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                imageBitmap = resizeImageBitmap(mImagePath);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageBitmap(imageBitmap);

                // Save image to gallery
                File file = new File(mImagePath);
                mImageUri = Uri.fromFile(file);
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(mImageUri);
                requireActivity().sendBroadcast(intent);
                break;
            case STORAGE_REQUEST_CODE:
                if (data != null) {
                    mImageUri = data.getData();
                    mImagePath = getImagePathFromUri(DetailPlanetFragment.this, mImageUri);

                    imageBitmap = resizeImageBitmap(mImagePath);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageBitmap(imageBitmap);
                }
                break;
            default:
                break;
        }
    }

    private void setUpInputFields(View view) {
        if (planet.getImage() != null) {
            mImageView.setVisibility(View.VISIBLE);
            mImageView.setImageBitmap(decodeImageFromString(planet.getImage()));
        }

        this.mNameLayout = view.findViewById(R.id.detail_planet_name_layout);
        this.mName = view.findViewById(R.id.detail_planet_name);
        this.mName.setText(planet.getName());
        this.mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNameLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.mDescriptionLayout = view.findViewById(R.id.detail_planet_description_layout);
        this.mDescription = view.findViewById(R.id.detail_planet_description);
        this.mDescription.setText(planet.getDescription());
        this.mDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDescriptionLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Spinner mStatus = view.findViewById(R.id.detail_planet_status);
        mStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mSelectedStatus = parentView.getItemAtPosition(position).toString();
            }

            public void onNothingSelected(AdapterView<?> parentView) {
                mSelectedStatus = null;
            }
        });

        // set adapter for mStatus spinner
        ArrayAdapter<CharSequence> dataAdapterStatus = ArrayAdapter.createFromResource(requireContext(),
                R.array.planet_status, android.R.layout.simple_spinner_item);
        dataAdapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStatus.setAdapter(dataAdapterStatus);
        mStatus.setSelection(Arrays.asList((getResources().getStringArray(R.array.planet_status))).indexOf(planet.getStatus()));

        mCrew = view.findViewById(R.id.detail_planet_crew);
        mCrew.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mSelectedCrew = parentView.getItemAtPosition(position).toString();
            }

            public void onNothingSelected(AdapterView<?> parentView) {
                mSelectedCrew = null;
            }
        });

        // set adapter for mCrew spinner
        ArrayAdapter<String> dataAdapterCrew = new ArrayAdapter<>(requireContext(),
                android.R.layout.simple_spinner_item, captains);
        dataAdapterCrew.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCrew.setAdapter(dataAdapterCrew);
    }

    private void setUpButtons(final View view) {
        Button btn_createImage = view.findViewById(R.id.btn_create_image);
        btn_createImage.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);
            showSelectImageDialog();
        });

        Button btn_update = view.findViewById(R.id.btn_update);
        btn_update.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);

            String crewId = null;
            boolean doUpdate = false;
            Planet updatePlanet = new Planet();

            if (mName.getText() != null && !mName.getText().toString().equals(planet.getName())) {
                updatePlanet.setName(mName.getText().toString());
                doUpdate = true;
            }

            if (mDescription.getText() != null && !mDescription.getText().toString().equals(planet.getDescription())) {
                updatePlanet.setDescription(mDescription.getText().toString());
                doUpdate = true;
            }

            if (!mSelectedStatus.equals(planet.getStatus()) &&
                    !mSelectedStatus.equals(getResources().getStringArray(R.array.planet_status)[0])) {
                updatePlanet.setStatus(mSelectedStatus);
                doUpdate = true;
            }

            if (mImageView.getVisibility() == View.VISIBLE && mImagePath != null) {
                String encodedImage = encodeImageToString(mImagePath);

                if (!planet.getImage().equals(encodedImage)) {
                    updatePlanet.setImage(encodedImage);
                    doUpdate = true;
                }
            }

            if ((planet.getExpeditionCrewDTO() == null && !mSelectedCrew.equals(captains.get(0))) ||
                    (planet.getExpeditionCrewDTO() != null &&
                            !mSelectedCrew.equals(planet.getExpeditionCrewDTO().getCaptain()) &&
                            !mSelectedCrew.equals(captains.get(0)))) {
                Crew crew = crews.stream().filter(c -> c.getUser().getName().equals(mSelectedCrew))
                        .findAny().orElse(new Crew());
                crewId = crew.getId();
                doUpdate = true;
            }

            if (doUpdate) {
                mScroll.pageScroll(View.FOCUS_UP);
                mNameLayout.setError(null);
                mDescriptionLayout.setError(null);

                updatePlanetCall(planet.getId(), crewId, updatePlanet);
            }
        });

        Button btn_delete = view.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);
            deletePlanetCall(planet.getId());
        });

        Button btn_back = view.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(v -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, ListPlanetsFragment.newInstance())
                .commit());
    }

    private void updatePlanetCall(final String id, final String crewId, final Planet updatePlanet) {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<Planet> call = planetApi.update(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(),
                id, crewId, updatePlanet);
        call.enqueue(new Callback<Planet>() {
            @Override
            public void onResponse(@NonNull Call<Planet> call, @NonNull Response<Planet> response) {
                if (response.isSuccessful()) {
                    planet = response.body();
                    Toast.makeText(requireActivity(), R.string.planet_is_updated, Toast.LENGTH_SHORT).show();
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Planet> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });

        refreshAccessTokenCall(authorizationToken);
    }

    private void deletePlanetCall(final String id) {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<Void> call = planetApi.delete(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(), id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(requireActivity(), R.string.planet_is_deleted, Toast.LENGTH_SHORT).show();

                    requireActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, ListPlanetsFragment.newInstance())
                            .commit();
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });

        refreshAccessTokenCall(authorizationToken);
    }

    private void getAllCrewCall() {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<List<Crew>> call = crewApi.getAll(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken());
        call.enqueue(new Callback<List<Crew>>() {
            @Override
            public void onResponse(@NonNull Call<List<Crew>> call, @NonNull Response<List<Crew>> response) {
                if (response.isSuccessful()) {
                    crews = response.body();

                    if (crews != null) {
                        captains.addAll(crews.stream().map(crew -> crew.getUser().getName()).collect(Collectors.toList()));
                    }

                    if (planet.getExpeditionCrewDTO() != null) {
                        mCrew.setSelection(captains.indexOf(planet.getExpeditionCrewDTO().getCaptain()));
                    } else {
                        mCrew.setSelection(0);
                    }
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Crew>> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void refreshAccessTokenCall(final AuthorizationToken authorizationToken) {
        Call<AuthorizationToken> call = authorizationTokenApi.refresh(authorizationToken);
        call.enqueue(new Callback<AuthorizationToken>() {
            @Override
            public void onResponse(@NonNull Call<AuthorizationToken> call,
                                   @NonNull Response<AuthorizationToken> response) {
                if (response.isSuccessful()) {
                    AuthorizationToken token = response.body();
                    if (token != null) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        SharedPreferencesUtil.setAuthTokenValue(requireActivity(), token);
                    }
                } else {
                    if (response.errorBody() != null) {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AuthorizationToken> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showSelectImageDialog() {
        final String[] options = {getString(R.string.camera), getString(R.string.gallery), getString(R.string.close)};

        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.select_image)
                .setItems(options, (dialog, which) -> {
                    if (options[which].equals(getString(R.string.camera))) {
                        if (hasCameraPermission(DetailPlanetFragment.this)) {
                            openCamera(DetailPlanetFragment.this);
                        } else {
                            requestCameraPermission(DetailPlanetFragment.this);
                        }
                    } else if (options[which].equals(getString(R.string.gallery))) {
                        if (hasStoragePermission(DetailPlanetFragment.this)) {
                            openStorage(DetailPlanetFragment.this);
                        } else {
                            requestStoragePermission(DetailPlanetFragment.this);
                        }
                    } else if (options[which].equals(getString(R.string.close))) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }
}
