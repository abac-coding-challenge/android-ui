package com.example.client.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.Robot;
import com.example.client.network.AuthorizationTokenApi;
import com.example.client.network.RobotApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.activity.SignInActivity;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_VALUE;
import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromFragment;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class AddRobotFragment extends Fragment {
    private RobotApi robotApi;
    private AuthorizationTokenApi authorizationTokenApi;

    private ScrollView mScroll;
    private TextInputLayout mNameLayout;
    private TextInputEditText mName;

    public AddRobotFragment() {
    }

    public static AddRobotFragment newInstance() {
        return new AddRobotFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_robot, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.robotApi = ServerAccessManager.getServerAccess().create(RobotApi.class);
        this.authorizationTokenApi = ServerAccessManager.getServerAccess().create(AuthorizationTokenApi.class);

        this.mScroll = view.findViewById(R.id.add_robot_scroll);

        setUpInputFields(view);
        setUpButtons(view);
        hideKeyboardFromFragment(requireContext(), view);
    }

    private void setUpInputFields(View view) {
        this.mNameLayout = view.findViewById(R.id.add_robot_name_layout);
        this.mName = view.findViewById(R.id.add_robot_name);
        this.mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNameLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setUpButtons(final View view) {
        Button btn_save = view.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);
            Robot robot = new Robot();

            if (mName.getText() != null && !mName.getText().toString().equals("")) {
                robot.setName(mName.getText().toString());
                addRobotCall(robot);
            } else {
                mNameLayout.setError(getText(R.string.error_invalid_name));
                mName.requestFocus();
            }
        });

        Button btn_clear = view.findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);

            new AlertDialog.Builder(view.getContext())
                    .setTitle(R.string.warning_clear)
                    .setMessage(R.string.warning_unsaved)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> clearInputs())
                    .setNegativeButton(android.R.string.no, null)
                    .show();
        });
    }

    private void addRobotCall(Robot robot) {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<Robot> call = robotApi.create(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(),
                authorizationToken.getUserId(), robot);
        call.enqueue(new Callback<Robot>() {
            @Override
            public void onResponse(@NonNull Call<Robot> call, @NonNull Response<Robot> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(requireActivity(), R.string.robot_is_saved, Toast.LENGTH_SHORT).show();
                    clearInputs();
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Robot> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });

        refreshAccessTokenCall(authorizationToken);
    }

    private void refreshAccessTokenCall(final AuthorizationToken authorizationToken) {
        Call<AuthorizationToken> call = authorizationTokenApi.refresh(authorizationToken);
        call.enqueue(new Callback<AuthorizationToken>() {
            @Override
            public void onResponse(@NonNull Call<AuthorizationToken> call,
                                   @NonNull Response<AuthorizationToken> response) {
                if (response.isSuccessful()) {
                    AuthorizationToken token = response.body();
                    if (token != null) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        SharedPreferencesUtil.setAuthTokenValue(requireActivity(), token);
                    }
                } else {
                    if (response.errorBody() != null) {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AuthorizationToken> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearInputs() {
        mScroll.pageScroll(View.FOCUS_UP);
        mNameLayout.setError(null);

        if (mName != null && mName.getText() != null) {
            mName.getText().clear();
            mName.clearFocus();
        }
    }
}
