package com.example.client.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.client.R;
import com.example.client.model.User;
import com.example.client.network.ServerAccessManager;
import com.example.client.network.UserApi;
import com.example.client.util.ExceptionConverterUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromActivity;
import static com.example.client.validator.UserValidator.validateEmail;
import static com.example.client.validator.UserValidator.validatePassword;
import static com.example.client.validator.UserValidator.validatePasswordConfirm;


public class SignUpActivity extends AppCompatActivity {
    private UserApi userApi;
    private View mProgressDialog;
    private View mSignUpForm;
    private TextInputLayout mNameLayout, mEmailLayout, mPasswordLayout, mPasswordConfirmLayout;
    private TextInputEditText mName, mEmail, mPassword, mPasswordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        hideKeyboardFromActivity(this);

        this.userApi = ServerAccessManager.getServerAccess().create(UserApi.class);
        this.mProgressDialog = findViewById(R.id.sign_up_progress);
        this.mSignUpForm = findViewById(R.id.sign_up_form);

        setUpInputFields();
        setUpButtons();
    }

    private void setUpInputFields() {
        this.mNameLayout = findViewById(R.id.sign_up_name_layout);
        this.mEmailLayout = findViewById(R.id.sign_up_email_layout);
        this.mPasswordLayout = findViewById(R.id.sign_up_password_layout);
        this.mPasswordConfirmLayout = findViewById(R.id.sign_up_password_confirm_layout);

        this.mName = findViewById(R.id.sign_up_name);
        this.mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mEmailLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.mEmail = findViewById(R.id.sign_up_email);
        this.mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mEmailLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.mPassword = findViewById(R.id.sign_up_password);
        this.mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPasswordLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.mPasswordConfirm = findViewById(R.id.sign_up_password_confirm);
        this.mPasswordConfirm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPasswordConfirmLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setUpButtons() {
        Button btn_signUp = findViewById(R.id.btn_sign_up);
        btn_signUp.setOnClickListener(v -> {
            boolean validInputs = true;

            if (mPasswordConfirm.getText() == null || mPassword.getText() == null ||
                    !validatePasswordConfirm(mPassword.getText().toString(), mPasswordConfirm.getText().toString())) {
                validInputs = false;
                mPasswordConfirmLayout.setError(getText(R.string.error_invalid_passwords));
                mPasswordConfirm.requestFocus();
            }

            if (mPassword.getText() == null || !validatePassword(mPassword.getText().toString())) {
                validInputs = false;
                mPasswordLayout.setError(getText(R.string.error_invalid_password));
                mPassword.requestFocus();
            }

            if (mEmail.getText() == null || !validateEmail(mEmail.getText().toString())) {
                validInputs = false;
                mEmailLayout.setError(getText(R.string.error_invalid_email));
                mEmail.requestFocus();
            }

            if (mName.getText() == null || mName.getText().toString().equals("")) {
                validInputs = false;
                mNameLayout.setError(getText(R.string.error_invalid_name));
                mName.requestFocus();
            }

            if (validInputs) {
                signUpCall(mName.getText().toString(), mEmail.getText().toString(), mPassword.getText().toString());
            }
        });

        Button btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(v -> SignUpActivity.super.onBackPressed());
    }

    private void signUpCall(final String name, final String email, final String password) {
        showProgressBar(true);
        User user = new User(null, null, email, password, name);
        Call<User> call = userApi.create(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                showProgressBar(false);

                if (response.isSuccessful()) {
                    User user = response.body();

                    if (user != null) {
                        showProgressBar(true);
                        startActivity(new Intent(SignUpActivity.this, SuccessfulRegistrationActivity.class));
                        finish();
                    }
                } else {
                    if (response.errorBody() != null) {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(SignUpActivity.this, exceptionMessage);
                        Toast.makeText(SignUpActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                showProgressBar(false);
                Toast.makeText(SignUpActivity.this, R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressBar(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);

        mSignUpForm.setVisibility(show ? View.GONE : View.VISIBLE);
        mSignUpForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSignUpForm.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

        mProgressDialog.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressDialog.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressDialog.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
    }
}
