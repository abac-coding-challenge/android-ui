package com.example.client.ui.fragment;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.client.R;
import com.example.client.model.Robot;

import java.util.ArrayList;
import java.util.List;

public class RobotCustomListView extends BaseAdapter {

    private Activity context;
    private List<Robot> robots;

    public RobotCustomListView(@NonNull Activity context) {
        this.context = context;
        this.robots = new ArrayList<>();
    }

    public RobotCustomListView(@NonNull Activity context, List<Robot> robots) {
        this.context = context;
        this.robots = robots;
    }

    @Override
    public int getCount() {
        return robots.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;

        if (view == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            view = layoutInflater.inflate(R.layout.listview_robot_layout, null, true);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.name.setText(robots.get(position).getName());

        return view;
    }

    static class ViewHolder {
        TextView name;
        ImageView image;

        ViewHolder(View view) {
            name = view.findViewById(R.id.list_robot_name);
            image = view.findViewById(R.id.list_robot_image);
        }
    }
}
