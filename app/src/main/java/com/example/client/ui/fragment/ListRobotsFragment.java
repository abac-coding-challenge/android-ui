package com.example.client.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.Robot;
import com.example.client.network.RobotApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.activity.SignInActivity;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_VALUE;
import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromFragment;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ListRobotsFragment extends Fragment {
    private RobotApi robotApi;
    private List<Robot> robots;
    private ListView mListView;
    private RobotCustomListView robotCustomListView;

    public ListRobotsFragment() {
    }

    public static ListRobotsFragment newInstance() {
        return new ListRobotsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_robots, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.robotApi = ServerAccessManager.getServerAccess().create(RobotApi.class);
        this.robots = new ArrayList<>();

        getAllRobotsByUserIdCall();
        hideKeyboardFromFragment(requireContext(), view);

        mListView = view.findViewById(R.id.list_robots);
        mListView.setOnItemClickListener((parent, view1, position, id) -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, DetailRobotFragment.newInstance(robots.get(position)))
                .addToBackStack(null)
                .commit());
        mListView.setEmptyView(view.findViewById(R.id.list_empty));
    }

    private void getAllRobotsByUserIdCall() {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<List<Robot>> call = robotApi.getAllByUserId(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(),
                authorizationToken.getUserId());
        call.enqueue(new Callback<List<Robot>>() {
            @Override
            public void onResponse(@NonNull Call<List<Robot>> call, @NonNull Response<List<Robot>> response) {
                if (response.isSuccessful()) {
                    robots = response.body();
                    robotCustomListView = new RobotCustomListView(requireActivity(), robots);
                    mListView.setAdapter(robotCustomListView);
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Robot>> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
