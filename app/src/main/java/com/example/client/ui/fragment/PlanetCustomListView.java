package com.example.client.ui.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.client.R;
import com.example.client.model.Planet;
import com.example.client.util.enums.Status;

import java.util.ArrayList;
import java.util.List;

import static com.example.client.util.ImageUtil.decodeImageFromString;
import static com.example.client.util.enums.Status.getEnumByValue;

public class PlanetCustomListView extends BaseAdapter {

    private Activity context;
    private List<Planet> planets;

    public PlanetCustomListView(@NonNull Activity context) {
        this.context = context;
        this.planets = new ArrayList<>();
    }

    public PlanetCustomListView(@NonNull Activity context, List<Planet> planets) {
        this.context = context;
        this.planets = planets;
    }

    @Override
    public int getCount() {
        return planets.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;

        if (view == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            view = layoutInflater.inflate(R.layout.listview_planet_layout, null, true);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.name.setText(planets.get(position).getName());
        viewHolder.status.setText(planets.get(position).getStatus());

        Status status = getEnumByValue(planets.get(position).getStatus());

        if (status != null) {
            switch (status) {
                case EN_ROUTE:
                    viewHolder.status.setTextColor(Color.GRAY);
                    break;
                case OK:
                    viewHolder.status.setTextColor(Color.GREEN);
                    break;
                case NOT_OK:
                    viewHolder.status.setTextColor(Color.RED);
                    break;
                case TODO:
                    viewHolder.status.setTextColor(Color.BLACK);
                    break;
                default:
            }
        }


        if (planets.get(position).getImage() != null) {
            viewHolder.image.setImageBitmap(decodeImageFromString(planets.get(position).getImage()));
        }

        String description = planets.get(position).getDescription();
        viewHolder.description.setText(description == null ?
                context.getString(R.string.list_planets_description) : description);

        String captain = null;
        String robots = null;

        if (planets.get(position).getExpeditionCrewDTO() != null) {
            captain = planets.get(position).getExpeditionCrewDTO().getCaptain();
            robots = planets.get(position).getCrewRobotsAsString();
        }

        viewHolder.captain.setText(captain == null ? "" :
                context.getString(R.string.list_planets_captain, captain));
        viewHolder.robots.setText(robots == null ? "" :
                context.getString(R.string.list_planets_robots, robots));

        return view;
    }

    static class ViewHolder {
        TextView name;
        ImageView image;
        TextView description;
        TextView status;
        TextView captain;
        TextView robots;

        ViewHolder(View view) {
            name = view.findViewById(R.id.list_planet_name);
            image = view.findViewById(R.id.list_planet_image);
            description = view.findViewById(R.id.list_planet_description);
            status = view.findViewById(R.id.list_planet_status);
            captain = view.findViewById(R.id.list_planet_captain);
            robots = view.findViewById(R.id.list_planet_robots);
        }
    }
}
