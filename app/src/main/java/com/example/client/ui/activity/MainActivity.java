package com.example.client.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.network.AuthorizationTokenApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.fragment.AddPlanetFragment;
import com.example.client.ui.fragment.AddRobotFragment;
import com.example.client.ui.fragment.ListPlanetsFragment;
import com.example.client.ui.fragment.ListRobotsFragment;
import com.example.client.util.SharedPreferencesUtil;
import com.google.android.material.navigation.NavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromActivity;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private AuthorizationTokenApi authorizationTokenApi;

    private DrawerLayout mDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideKeyboardFromActivity(this);

        // Check access token validity
        AuthorizationToken token = SharedPreferencesUtil.getAuthTokenValue(MainActivity.this);

        if (token == null || token.getAccessToken() == null || token.getAccessToken().equals("")) {
            SharedPreferencesUtil.removeSharedPref(MainActivity.this);
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
            finish();
        }

        this.authorizationTokenApi = ServerAccessManager.getServerAccess().create(AuthorizationTokenApi.class);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        this.mDrawer = findViewById(R.id.drawer_layout);

        setSupportActionBar(mToolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(MainActivity.this, mDrawer, mToolbar,
                R.string.nav_drawer_open, R.string.nav_drawer_close);

        this.mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new ListPlanetsFragment())
                    .commit();
            navigationView.setCheckedItem(R.id.nav_list_planet);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                mDrawer.closeDrawer(GravityCompat.START);
            } else {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.warning_close)
                        .setMessage(R.string.warning_unsaved)
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> MainActivity.super.onBackPressed())
                        .setNegativeButton(android.R.string.no, null)
                        .show();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_list_planet:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, ListPlanetsFragment.newInstance())
                        .commit();
                break;
            case R.id.nav_list_robot:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, ListRobotsFragment.newInstance())
                        .commit();
                break;
            case R.id.nav_add_planet:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, AddPlanetFragment.newInstance())
                        .commit();
                break;
            case R.id.nav_add_robot:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, AddRobotFragment.newInstance())
                        .commit();
                break;
            case R.id.nav_logout:
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.warning_sign_out)
                        .setMessage(R.string.warning_unsaved)
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> signOutCall())
                        .setNegativeButton(android.R.string.no, null)
                        .show();
                break;
        }

        mDrawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void signOutCall() {
        AuthorizationToken token = SharedPreferencesUtil.getAuthTokenValue(MainActivity.this);
        Call<Void> call = authorizationTokenApi.remove(token.getUserId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                SharedPreferencesUtil.removeSharedPref(MainActivity.this);
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                finish();
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                SharedPreferencesUtil.removeSharedPref(MainActivity.this);
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                finish();
            }
        });
    }
}