package com.example.client.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.Planet;
import com.example.client.network.PlanetApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.activity.SignInActivity;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_VALUE;
import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromFragment;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ListPlanetsFragment extends Fragment {
    private PlanetApi planetApi;
    private List<Planet> planets;
    private ListView mListView;
    private PlanetCustomListView planetCustomListView;

    public ListPlanetsFragment() {
    }

    public static ListPlanetsFragment newInstance() {
        return new ListPlanetsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_planets, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.planetApi = ServerAccessManager.getServerAccess().create(PlanetApi.class);
        this.planets = new ArrayList<>();

        getAllPlanetsCall();
        hideKeyboardFromFragment(requireContext(), view);

        mListView = view.findViewById(R.id.list_planets);
        mListView.setOnItemClickListener((parent, view1, position, id) -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, DetailPlanetFragment.newInstance(planets.get(position)))
                .addToBackStack(null)
                .commit());
        mListView.setEmptyView(view.findViewById(R.id.list_empty));
    }

    private void getAllPlanetsCall() {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<List<Planet>> call = planetApi.getAll(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken());
        call.enqueue(new Callback<List<Planet>>() {
            @Override
            public void onResponse(@NonNull Call<List<Planet>> call, @NonNull Response<List<Planet>> response) {
                if (response.isSuccessful()) {
                    planets = response.body();
                    planetCustomListView = new PlanetCustomListView(requireActivity(), planets);
                    mListView.setAdapter(planetCustomListView);
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Planet>> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
