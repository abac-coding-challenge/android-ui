package com.example.client.ui.activity;

import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.client.R;

import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromActivity;

public class SuccessfulRegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successful_registration);
        hideKeyboardFromActivity(this);

        Button btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(v -> SuccessfulRegistrationActivity.super.onBackPressed());
    }
}
