package com.example.client.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.Robot;
import com.example.client.network.AuthorizationTokenApi;
import com.example.client.network.RobotApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.activity.SignInActivity;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_VALUE;
import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromFragment;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class DetailRobotFragment extends Fragment {
    private static final String ROBOT = "ROBOT";

    private RobotApi robotApi;
    private AuthorizationTokenApi authorizationTokenApi;

    private Robot robot;
    private ScrollView mScroll;
    private TextInputLayout mNameLayout;
    private TextInputEditText mName;

    public DetailRobotFragment() {
    }

    public static DetailRobotFragment newInstance(Robot robot) {
        DetailRobotFragment fragment = new DetailRobotFragment();
        Bundle args = new Bundle();
        args.putSerializable(ROBOT, robot);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.robot = (Robot) getArguments().getSerializable(ROBOT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_robot, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.robotApi = ServerAccessManager.getServerAccess().create(RobotApi.class);
        this.authorizationTokenApi = ServerAccessManager.getServerAccess().create(AuthorizationTokenApi.class);

        this.mScroll = view.findViewById(R.id.detail_robot_scroll);

        setUpInputFields(view);
        setUpButtons(view);
        hideKeyboardFromFragment(requireContext(), view);
    }

    private void setUpInputFields(View view) {
        this.mNameLayout = view.findViewById(R.id.detail_robot_name_layout);
        this.mName = view.findViewById(R.id.detail_robot_name);
        this.mName.setText(robot.getName());
        this.mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNameLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void setUpButtons(final View view) {
        Button btn_update = view.findViewById(R.id.btn_update);
        btn_update.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);

            boolean doUpdate = false;
            Robot updateRobot = new Robot();

            if (mName.getText() != null && !mName.getText().toString().equals(robot.getName())) {
                updateRobot.setName(mName.getText().toString());
                doUpdate = true;
            }

            if (doUpdate) {
                mScroll.pageScroll(View.FOCUS_UP);
                mNameLayout.setError(null);

                updateRobotCall(robot.getId(), updateRobot);
            }
        });

        Button btn_delete = view.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);
            deleteRobotCall(robot.getId());
        });

        Button btn_back = view.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(v -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, ListRobotsFragment.newInstance())
                .commit());
    }

    private void updateRobotCall(final String id, final Robot updateRobot) {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<Robot> call = robotApi.update(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(),
                id, updateRobot);
        call.enqueue(new Callback<Robot>() {
            @Override
            public void onResponse(@NonNull Call<Robot> call, @NonNull Response<Robot> response) {
                if (response.isSuccessful()) {
                    robot = response.body();
                    Toast.makeText(requireActivity(), R.string.robot_is_updated, Toast.LENGTH_SHORT).show();
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Robot> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });

        refreshAccessTokenCall(authorizationToken);
    }

    private void deleteRobotCall(final String id) {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<Void> call = robotApi.delete(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(), id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(requireActivity(), R.string.robot_is_deleted, Toast.LENGTH_SHORT).show();

                    requireActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, ListRobotsFragment.newInstance())
                            .commit();
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });

        refreshAccessTokenCall(authorizationToken);
    }

    private void refreshAccessTokenCall(final AuthorizationToken authorizationToken) {
        Call<AuthorizationToken> call = authorizationTokenApi.refresh(authorizationToken);
        call.enqueue(new Callback<AuthorizationToken>() {
            @Override
            public void onResponse(@NonNull Call<AuthorizationToken> call,
                                   @NonNull Response<AuthorizationToken> response) {
                if (response.isSuccessful()) {
                    AuthorizationToken token = response.body();
                    if (token != null) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        SharedPreferencesUtil.setAuthTokenValue(requireActivity(), token);
                    }
                } else {
                    if (response.errorBody() != null) {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AuthorizationToken> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
