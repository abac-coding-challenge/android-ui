package com.example.client.ui.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.client.R;
import com.example.client.model.AuthorizationToken;
import com.example.client.model.Planet;
import com.example.client.network.AuthorizationTokenApi;
import com.example.client.network.PlanetApi;
import com.example.client.network.ServerAccessManager;
import com.example.client.ui.activity.SignInActivity;
import com.example.client.util.ExceptionConverterUtil;
import com.example.client.util.SharedPreferencesUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.example.client.util.Constants.HEADER_AUTHORIZATION_VALUE;
import static com.example.client.util.ExceptionConverterUtil.convertExceptionToMessage;
import static com.example.client.util.HideKeyboardUtil.hideKeyboardFromFragment;
import static com.example.client.util.ImageUtil.encodeImageToString;
import static com.example.client.util.ImageUtil.getImagePathFromFile;
import static com.example.client.util.ImageUtil.getImagePathFromUri;
import static com.example.client.util.ImageUtil.hasCameraPermission;
import static com.example.client.util.ImageUtil.hasStoragePermission;
import static com.example.client.util.ImageUtil.openCamera;
import static com.example.client.util.ImageUtil.openStorage;
import static com.example.client.util.ImageUtil.requestCameraPermission;
import static com.example.client.util.ImageUtil.requestStoragePermission;
import static com.example.client.util.ImageUtil.resizeImageBitmap;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class AddPlanetFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final int CAMERA_REQUEST_CODE = 1000;
    private static final int STORAGE_REQUEST_CODE = 1001;
    private static final int CAMERA_PERMISSION_CODE = 1888;
    private static final int STORAGE_PERMISSION_CODE = 1889;

    private PlanetApi planetApi;
    private AuthorizationTokenApi authorizationTokenApi;

    private ScrollView mScroll;
    private ImageView mImageView;
    private TextInputLayout mNameLayout, mDescriptionLayout;
    private TextInputEditText mName, mDescription;
    private Spinner mStatus;
    private String mSelectedStatus;

    private String mImagePath;

    public AddPlanetFragment() {
    }

    public static AddPlanetFragment newInstance() {
        return new AddPlanetFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_planet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.planetApi = ServerAccessManager.getServerAccess().create(PlanetApi.class);
        this.authorizationTokenApi = ServerAccessManager.getServerAccess().create(AuthorizationTokenApi.class);

        this.mScroll = view.findViewById(R.id.add_planet_scroll);
        this.mImageView = view.findViewById(R.id.add_planet_image_view);

        setUpInputFields(view);
        setUpButtons(view);
        hideKeyboardFromFragment(requireContext(), view);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted && writeStorageAccepted) {
                        openCamera(AddPlanetFragment.this);
                    } else {
                        Toast.makeText(requireActivity(), R.string.error_permission_denied, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case STORAGE_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (writeStorageAccepted) {
                        openStorage(AddPlanetFragment.this);
                    } else {
                        Toast.makeText(requireActivity(), R.string.error_permission_denied, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        Uri mImageUri;
        Bitmap imageBitmap;

        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                mImagePath = getImagePathFromFile();
                imageBitmap = resizeImageBitmap(mImagePath);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageBitmap(imageBitmap);

                // Save image to gallery
                File file = new File(mImagePath);
                mImageUri = Uri.fromFile(file);
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(mImageUri);
                requireActivity().sendBroadcast(intent);
                break;
            case STORAGE_REQUEST_CODE:
                if (data != null) {
                    mImageUri = data.getData();
                    mImagePath = getImagePathFromUri(AddPlanetFragment.this, mImageUri);

                    imageBitmap = resizeImageBitmap(mImagePath);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageBitmap(imageBitmap);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mSelectedStatus = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        mSelectedStatus = null;
    }

    private void setUpInputFields(View view) {
        this.mNameLayout = view.findViewById(R.id.add_planet_name_layout);
        this.mName = view.findViewById(R.id.add_planet_name);
        this.mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNameLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        this.mDescriptionLayout = view.findViewById(R.id.add_planet_description_layout);
        this.mDescription = view.findViewById(R.id.add_planet_description);
        this.mDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mDescriptionLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mStatus = view.findViewById(R.id.add_planet_status);
        mStatus.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> dataAdapterStatus = ArrayAdapter.createFromResource(requireContext(),
                R.array.planet_status, android.R.layout.simple_spinner_item);
        dataAdapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStatus.setAdapter(dataAdapterStatus);
    }

    private void setUpButtons(final View view) {
        Button btn_createImage = view.findViewById(R.id.btn_create_image);
        btn_createImage.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);
            showSelectImageDialog();
        });

        Button btn_save = view.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);

            boolean validInputs = true;
            Planet planet = new Planet();

            if (mName.getText() != null && !mName.getText().toString().equals("")) {
                planet.setName(mName.getText().toString());
            } else {
                validInputs = false;
                mNameLayout.setError(getText(R.string.error_invalid_name));
                mName.requestFocus();
            }

            if (mImageView.getVisibility() == View.VISIBLE && mImagePath != null) {
                planet.setImage(encodeImageToString(mImagePath));
            } else {
                validInputs = false;
                Toast.makeText(requireActivity(), R.string.error_empty_image, Toast.LENGTH_SHORT).show();
            }

            if (mDescription.getText() != null && !mDescription.getText().toString().equals("")) {
                planet.setDescription(mDescription.getText().toString());
            }

            if (mSelectedStatus != null && !mSelectedStatus.equals("") &&
                    !mSelectedStatus.equals(getResources().getStringArray(R.array.planet_status)[0])) {
                planet.setStatus(mSelectedStatus);
            } else {
                validInputs = false;
                Toast.makeText(requireActivity(), R.string.error_empty_status, Toast.LENGTH_SHORT).show();
            }

            if (validInputs) {
                addPlanetCall(planet);
            }
        });

        Button btn_clear = view.findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(v -> {
            hideKeyboardFromFragment(requireContext(), view);

            new AlertDialog.Builder(view.getContext())
                    .setTitle(R.string.warning_clear)
                    .setMessage(R.string.warning_unsaved)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> clearInputs())
                    .setNegativeButton(android.R.string.no, null)
                    .show();
        });
    }

    private void clearInputs() {
        mImageView.setImageResource(0);
        mImageView.setVisibility(View.GONE);
        mImagePath = null;

        mScroll.pageScroll(View.FOCUS_UP);

        mNameLayout.setError(null);
        mDescriptionLayout.setError(null);

        if (mName != null && mName.getText() != null) {
            mName.getText().clear();
            mName.clearFocus();
        }

        if (mDescription != null && mDescription.getText() != null) {
            mDescription.getText().clear();
            mDescription.clearFocus();
        }

        mSelectedStatus = null;
        mStatus.setSelection(0);
    }

    private void addPlanetCall(final Planet planet) {
        AuthorizationToken authorizationToken = SharedPreferencesUtil.getAuthTokenValue(requireActivity());

        Call<Planet> call = planetApi.create(HEADER_AUTHORIZATION_VALUE + authorizationToken.getAccessToken(), planet);
        call.enqueue(new Callback<Planet>() {
            @Override
            public void onResponse(@NonNull Call<Planet> call, @NonNull Response<Planet> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(requireActivity(), R.string.planet_is_saved, Toast.LENGTH_SHORT).show();
                    clearInputs();
                } else {
                    if (response.code() == HTTP_UNAUTHORIZED || response.code() == HTTP_FORBIDDEN) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        Toast.makeText(requireActivity(), R.string.error_access_denied, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(requireActivity(), SignInActivity.class));
                        requireActivity().finish();
                    } else {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Planet> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });

        refreshAccessTokenCall(authorizationToken);
    }

    private void refreshAccessTokenCall(final AuthorizationToken authorizationToken) {
        Call<AuthorizationToken> call = authorizationTokenApi.refresh(authorizationToken);
        call.enqueue(new Callback<AuthorizationToken>() {
            @Override
            public void onResponse(@NonNull Call<AuthorizationToken> call,
                                   @NonNull Response<AuthorizationToken> response) {
                if (response.isSuccessful()) {
                    AuthorizationToken token = response.body();
                    if (token != null) {
                        SharedPreferencesUtil.removeSharedPref(requireActivity());
                        SharedPreferencesUtil.setAuthTokenValue(requireActivity(), token);
                    }
                } else {
                    if (response.errorBody() != null) {
                        String exceptionMessage = convertExceptionToMessage(response.errorBody());
                        String errorMessage = ExceptionConverterUtil.getErrorMessage(requireActivity(), exceptionMessage);
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AuthorizationToken> call, @NonNull Throwable t) {
                Toast.makeText(requireActivity(), R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showSelectImageDialog() {
        final String[] options = {getString(R.string.camera), getString(R.string.gallery), getString(R.string.close)};

        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.select_image)
                .setItems(options, (dialog, which) -> {
                    if (options[which].equals(getString(R.string.camera))) {
                        if (hasCameraPermission(AddPlanetFragment.this)) {
                            openCamera(AddPlanetFragment.this);
                        } else {
                            requestCameraPermission(AddPlanetFragment.this);
                        }
                    } else if (options[which].equals(getString(R.string.gallery))) {
                        if (hasStoragePermission(AddPlanetFragment.this)) {
                            openStorage(AddPlanetFragment.this);
                        } else {
                            requestStoragePermission(AddPlanetFragment.this);
                        }
                    } else if (options[which].equals(getString(R.string.close))) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }
}
