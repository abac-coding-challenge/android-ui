package com.example.client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Crew implements Serializable {
    private String id;
    private User user;
    private List<Robot> robots;

    public Crew() {
        robots = new ArrayList<>();
    }

    public Crew(String id, User user, List<Robot> robots) {
        this.id = id;
        this.user = user;
        this.robots = robots;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Robot> getRobots() {
        return robots;
    }
}
