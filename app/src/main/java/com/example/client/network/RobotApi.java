package com.example.client.network;

import com.example.client.model.Robot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_KEY;

public interface RobotApi {

    @GET("/api/robots/{userId}")
    Call<List<Robot>> getAllByUserId(@Header(HEADER_AUTHORIZATION_KEY) String token, @Path("userId") String userId);

    @POST("/api/robots/{userId}")
    Call<Robot> create(@Header(HEADER_AUTHORIZATION_KEY) String token,
                       @Path("userId") String userId,
                       @Body Robot robot);

    @PATCH("/api/robots/{id}")
    Call<Robot> update(@Header(HEADER_AUTHORIZATION_KEY) String token,
                       @Path("id") String id,
                       @Body Robot robot);

    @DELETE("/api/robots/{id}")
    Call<Void> delete(@Header(HEADER_AUTHORIZATION_KEY) String token, @Path("id") String id);
}
