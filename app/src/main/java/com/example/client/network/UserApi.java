package com.example.client.network;

import com.example.client.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserApi {

    @POST("/api/users")
    Call<User> create(@Body User user);
}
