package com.example.client.network;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.client.util.Constants.BASE_URL;
import static com.example.client.util.Constants.HEADER_CONTENT_TYPE_KEY;
import static com.example.client.util.Constants.HEADER_CONTENT_TYPE_VALUE;

public class ServerAccessManager {
    private static Retrofit serverAccess;

    private ServerAccessManager() {
    }

    public static Retrofit getServerAccess() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader(HEADER_CONTENT_TYPE_KEY, HEADER_CONTENT_TYPE_VALUE)
                        .build();
                return chain.proceed(request);
            }
        });

        if (serverAccess == null) {
            serverAccess = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }

        return serverAccess;
    }
}
