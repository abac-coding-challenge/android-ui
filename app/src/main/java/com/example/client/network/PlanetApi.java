package com.example.client.network;

import com.example.client.model.Planet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_KEY;

public interface PlanetApi {

    @GET("/api/planets")
    Call<List<Planet>> getAll(@Header(HEADER_AUTHORIZATION_KEY) String token);

    @POST("/api/planets")
    Call<Planet> create(@Header(HEADER_AUTHORIZATION_KEY) String token, @Body Planet planet);

    @PATCH("/api/planets/{id}")
    Call<Planet> update(@Header(HEADER_AUTHORIZATION_KEY) String token,
                        @Path("id") String id,
                        @Query("crewId") String crewId,
                        @Body Planet planet);

    @DELETE("/api/planets/{id}")
    Call<Void> delete(@Header(HEADER_AUTHORIZATION_KEY) String token, @Path("id") String id);
}
