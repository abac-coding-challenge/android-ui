package com.example.client.network;

import com.example.client.model.Crew;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

import static com.example.client.util.Constants.HEADER_AUTHORIZATION_KEY;

public interface CrewApi {

    @GET("/api/crews")
    Call<List<Crew>> getAll(@Header(HEADER_AUTHORIZATION_KEY) String token);
}
