package com.example.client.network;

import com.example.client.model.AuthorizationToken;
import com.example.client.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AuthorizationTokenApi {
    @POST("/api/tokens")
    Call<AuthorizationToken> create(@Body User user);

    @POST("/api/tokens/refresh")
    Call<AuthorizationToken> refresh(@Body AuthorizationToken token);

    @DELETE("/api/tokens/{userId}")
    Call<Void> remove(@Path("userId") String userId);
}
